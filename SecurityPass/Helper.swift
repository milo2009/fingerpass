//
//  Helper.swift
//  FingerPass
//
//  Created by Fred on 10/04/2015.
//  Copyright (c) 2015 Fred. All rights reserved.
//

import UIKit

class Helper {

    class func createDefaultAlert(title: String, message: String?, defaultAction: Bool = true) -> UIAlertController{
        let alert = UIAlertController(title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.Alert)
        
        if(defaultAction == true){
            alert.addAction(UIAlertAction(title: "OK",
                style: .Default,
                handler: nil))
        }
        
        return alert
    }
}