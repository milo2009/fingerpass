//
//  HomeController.swift
//  FingerPass
//
//  Created by Fred on 09/04/2015.
//  Copyright (c) 2015 Fred. All rights reserved.
//

import UIKit
import CoreData

private let kCellIdentifier : String = "cellPass"
private var codes = NSMutableArray()
private var securePass = true

class HomeController: UITableViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "lock"),
            style: .Plain, target: self, action: "showHidePassword")
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        
        let fetchRequest = NSFetchRequest(entityName:"Code")
        
        var error: NSError?
        let fetchedResults = managedContext.executeFetchRequest(fetchRequest,
            error: &error) as! [NSManagedObject]?
        
        if let results = fetchedResults {
            codes = NSMutableArray(array: results)
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
        
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showHidePassword(){
        securePass = !securePass
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: (securePass) ? "lock" : "unlock"),
            style: .Plain, target: self, action: "showHidePassword")
        
        tableView.reloadData()
    }
    
    //MARK: TableView

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return codes.count;
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier(kCellIdentifier) as! UITableViewCell
        
        let code: AnyObject = codes[indexPath.row]
        
        var name: UILabel = cell.contentView.viewWithTag(1) as! UILabel
        name.text = code.name
        
        let passCode = code.valueForKey("password") as! NSData
        var pass: UITextField = cell.contentView.viewWithTag(2) as! UITextField
        pass.secureTextEntry = securePass
        pass.text = NSString(data: passCode, encoding: NSUTF8StringEncoding) as String!
        
        var icon: UIImageView = cell.contentView.viewWithTag(3) as! UIImageView
        icon.image = UIImage(named: code.valueForKey("icon") as! String)
        
        return cell
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            let appDel = UIApplication.sharedApplication().delegate as! AppDelegate
            let context = appDel.managedObjectContext!
            context.deleteObject(codes[indexPath.row] as! NSManagedObject)
            codes.removeObjectAtIndex(indexPath.row)
            context.save(nil)
            
            // remove the deleted item from the `UITableView`
            self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            
        }
    }
    
    //MARK: Textfield
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let alert = Helper.createDefaultAlert("Edition interdite",
            message: "Vous ne pouvez que copier le code",
            defaultAction: true)
        self.presentViewController(alert, animated: true, completion: nil)
        
        return false
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        
        return false;
    }
}

