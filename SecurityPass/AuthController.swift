//
//  AuthController.swift
//  FingerPass
//
//  Created by Fred on 09/04/2015.
//  Copyright (c) 2015 Fred. All rights reserved.
//

import UIKit
import LocalAuthentication

class AuthController: UIViewController, UITextFieldDelegate {
    private var typedPass = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.authentification()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func authentification() {
        var touchIDContext = LAContext()
        // On ne veux pas gerer un systeme de code
        touchIDContext.localizedFallbackTitle = ""
        var touchIDError : NSError?
        var reasonString = "Authentification requise"
        
        // On vérifie si on peux utiliser le Touch ID
        if touchIDContext.canEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, error:&touchIDError) {
            // On vérifie si l'emprinte est reconnue
            touchIDContext.evaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString, reply: {
                (success: Bool, error: NSError!) -> Void in
                if success {
                    // Authentification réussi
                    self.pushToHomeView()
                } else {                    
                    // Authentification échouée
                    let message : String
                    switch(error.code) {
                    case LAError.AuthenticationFailed.rawValue:
                        message = "Impossible de vérifier votre identité"
                    case LAError.UserCancel.rawValue:
                        message = "Authentification par code"
                    case LAError.UserFallback.rawValue:
                        message = "Authentification par code"
                    default:
                        message = "Touch ID non configuré"
                    }
                    
                    self.showPasswordAlert("Touch ID non disponible")
                }
            })
            
            
        } else {
            // Il n'y a pas de Touch ID
            self.showPasswordAlert("Touch ID non disponible")
        }
    }
    
    func pushToHomeView(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("tabController") as! UIViewController
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        typedPass = textField.text + string
        println(typedPass)
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.passwordIsValid(textField.text)
        return true
    }
    
    func passwordIsValid(typedPassword : String){
        if(typedPassword == "pass"){
            self.pushToHomeView()
        }
        else{
            self.alertExitApp()
        }
    }
    
    func showPasswordAlert(title : String) {
        let actionSheetController = Helper.createDefaultAlert(title,
            message: "Enter your password",
            defaultAction: false)
        
        //Create and add the Cancel action
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            self.alertExitApp()
        }
        actionSheetController.addAction(cancelAction)
        
        //Create and an option action
        let loginAction = UIAlertAction(title: "Login", style: .Default) { action -> Void in
            self.passwordIsValid(self.typedPass)
        }
        actionSheetController.addAction(loginAction)
        
        //Add a text field
        actionSheetController.addTextFieldWithConfigurationHandler { textField -> Void in
            //TextField configuration
            textField.delegate = self
            textField.textColor = UIColor.blueColor()
        }
        
        //Present the AlertController
        self.presentViewController(actionSheetController, animated: true, completion: nil)
    }
    
    func alertExitApp(){
        let alert = Helper.createDefaultAlert("Identification impossible",
            message: "L'application va se fermer", defaultAction: false)
        alert.addAction(UIAlertAction(title: "OK",
            style: .Default,
            handler:{(alert: UIAlertAction!) in
                exit(0)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

