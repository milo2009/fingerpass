//
//  Code.h
//  FingerPass
//
//  Created by Fred on 23/06/2015.
//  Copyright (c) 2015 Fred. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Code : NSManagedObject

@property (nonatomic, retain) NSString * icon;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSData * password;

@end
