//
//  AddController.swift
//  FingerPass
//
//  Created by Fred on 09/04/2015.
//  Copyright (c) 2015 Fred. All rights reserved.
//

import UIKit
import CoreData

private let reuseIdentifier = "ImageCell"
private var iconSel = "none"

class AddController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var tvPass: UITextField!
    @IBOutlet weak var tvName: UITextField!
    var images : NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        var path = NSBundle.mainBundle().pathForResource("imageList", ofType: "plist")
        
        if (path != nil){
            let myDict = NSDictionary(contentsOfFile: path!)
            if let types = myDict?.valueForKey("IcoPass") as? NSArray {
                images = types;
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: privates
    func save() {
        if((count(tvName.text) == 0) || (count(tvPass.text) == 0)){
            let alert = Helper.createDefaultAlert("Ajout annulé",
                message: "Certains champs ne sont pas renseignés")
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        //1
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        
        //On verifie qu'un nom identifique n'existe pas deja
        let fetchRequest = NSFetchRequest(entityName:"Code")
        fetchRequest.predicate = NSPredicate(format: "name = %@", tvName.text)
        
        var results:NSArray = managedContext.executeFetchRequest(fetchRequest, error: nil)!
        if(results.count > 0){
            let alert = Helper.createDefaultAlert("Ajout annulé",
                message: "Un code portant le même nom existe déjà")
            self.presentViewController(alert, animated: true, completion: nil)
            
            return;
        }
        
        //Creation
        let entity =  NSEntityDescription.entityForName("Code",
            inManagedObjectContext:
            managedContext)
        
        let code = NSManagedObject(entity: entity!,
            insertIntoManagedObjectContext:managedContext)
        
        code.setValue(tvName.text, forKey: "name")
        code.setValue(tvPass.text.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false),
            forKey: "password")
        code.setValue(iconSel, forKey: "icon")
        
        var error: NSError?
        if !managedContext.save(&error) {
            println("Could not save \(error), \(error?.userInfo)")
            
            let alert = Helper.createDefaultAlert("Ajout impossible",
                message: "Une erreur est survenue")
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else{
            self.tabBarController?.selectedIndex = 0;
        }
    }

    // MARK: UITextField
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return false;
    }
    
    // MARK: UICollectionViewDataSource
    
    //1
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //2
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    //3
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! UICollectionViewCell
        
        // Configure the cell
        cell.tag = indexPath.row
        
        let img = cell.contentView.viewWithTag(1) as! UIImageView
        img.image = UIImage(named: images[indexPath.row] as! String)
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        tvName.resignFirstResponder();
        tvPass.resignFirstResponder();
        
        var selCell = collectionView.cellForItemAtIndexPath(indexPath)
        for cell in collectionView.visibleCells(){
            if(cell.tag == selCell?.tag){
                cell.layer.borderColor = UIColor.blueColor().CGColor
                cell.layer.borderWidth = 1
            }
            else{
                cell.layer.borderWidth = 0
            }
        }

        iconSel = images[indexPath.row] as! String
        self.save()
    }
}

